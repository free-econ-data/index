---
pagetitle: "Free econ-data"
title: "Semana 6: Operaciones con valores basadas en condiciones"
subtitle: Taller de Excel | [ECON-1300](https://bloqueneon.uniandes.edu.co/d2l/home/133085)
author: 
      name: Eduard-Martínez
      affiliation: Universidad de los Andes  #[`r fontawesome::fa('globe')`]()
# date: Lecture 10  #"`r format(Sys.time(), '%d %B %Y')`"
output: 
  html_document:
    theme: flatly
    highlight: haddock
    # code_folding: show
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    # fig_width: 7 ## Optional: Set default PDF figure width
    # fig_height: 6 ## Optional: Set default PDF figure height
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome,kableExtra)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!--==================-->
<!--==================-->
## Prueba
